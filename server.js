const Koa = require('koa');
const router = require('koa-router')();
const bodyParser = require('koa-bodyparser')
var session = require('koa-session');

const src = require('./src/routers/app');

const app = new Koa();

app.keys = ['wuju'];

app.use(session({
　　keys : 'koa:sess',
　　maxAge : 20000000
},app));

app.use(bodyParser());

app.use(src.routes());


app.use(router.allowedMethods());

app.listen(3000,() => {
	console.log('starting at port 3000');
});