const { DataTypes } = require('sequelize');

const sequelize = require('../../db/connect');

const User = sequelize.define('u', {
	id: {
		type: DataTypes.INTEGER, // 类型
		primaryKey: true // 键
	},
	username: {
		type: DataTypes.STRING,
		field: 'username'
	},
	password : {
		type: DataTypes.STRING,
		field: 'password'
	},
	email : {
		type: DataTypes.STRING,
		field: 'email'
	},
	nickname : {
		type: DataTypes.STRING,
		field: 'nickname'
	},
	token : {
		type: DataTypes.STRING,
		field: 'token'
	},
	cTime : {
		type: DataTypes.INTEGER,
		field: 'token'
	}
},{
	// 表名
	tableName: 'i_users',
	timestamps: false // 不添加时间字段
});

module.exports = User;