const router = require('koa-router')();

const User = require('../../db/user');

router.post('/login',async ctx => {
	let query = ctx.request.body;
	var user;
	try {
		user = await User.find(query);
	} catch(e) {
		ctx.body = {
			code: 400,
			msg: '登录失败-0'
		};
		return false;
	}
	if(user == undefined) {
		ctx.body = {
			code: 400,
			msg: '登录失败-1'
		};
		return false;
	}
	if(user.password != query.password) {
		ctx.body = {
			code: 400,
			msg: '登录失败-2'
		};
		return false;
	}
	ctx.body = {
		code: 200,
		msg: '登录成功'
	};
});


router.get('/test',ctx => {
	let { code } = ctx.request.query;
	console.log(code , ctx.session.code)
	if(code == ctx.session.code) {
		ctx.body = true;
	} else {
		ctx.body = false;
	}
})

router.post('/regis',async ctx => {
	let query = ctx.request.body;
	var user;
	try {
		data = await User.add(query);
		ctx.body = {
			code : 200,
			msg : '注册成功',
			data
		}
	} catch(e) {
		ctx.body = {
			code : 404,
			msg : '注册失败'
		}
	}
})

module.exports = router.routes();