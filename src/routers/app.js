const router = require('koa-router')();

var user = require('../app/user/index');

// 验证码
router.get('/code', ctx => {
	var captcha = require('svg-captcha').create({
		size : 4,//验证码个数
		charPreset : 'qwertyuiopasdfghjklzxcvbnm1234567890',
		fontSize : 50,//验证码字体大小
		width : 100,//宽度
		heigth : 40,//高度
		background:'#f0f'//背景大小
	});

	ctx.session.code = captcha.text;
	ctx.response.type = 'image/svg+xml';
	ctx.body = captcha.data;

});

router.use('/user',user);

module.exports = router;