const { Sequelize } = require('sequelize');

const config = require('../../config');

console.log(config)

var sequelize;
// 测试连接
try {
	sequelize = new Sequelize(config['DB'].database,
	config['DB'].username,
	config['DB'].password, {
		host: config['DB'].host,
		dialect: 'mysql',
		// logging: false, // 取消日志
	});
	console.log('数据库连接成功');
} catch (error) {
	console.error('数据库连接失败，异常信息:', error);
}

module.exports = sequelize;