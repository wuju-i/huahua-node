const models = require('../app/user/models');

class User {
	static async find(query) {

		let ret = await models.findOne({
			where: {
				...query
			}
		});

		return JSON.parse(JSON.stringify(ret));
	}

	static async add(query) {

		return await models.create({
            username : query.username,
            password : query.password,
            nickname : query.nickname,
            token : 'AAAAAAAAAAAAAA',
            cTime : Date.now()
        })
	}

}

module.exports = User;
